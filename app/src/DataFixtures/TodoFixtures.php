<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Todo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TodoFixtures extends Fixture implements DependentFixtureInterface
{
    public const NUM = 150;
    public const REFERENCE_PREFIX = 'todo';

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $projects = iterator_to_array($this->getProjects());

        for ($i = 0; $i < self::NUM; ++$i) {
            $todo = new Todo();
            $todo->setName(trim($faker->sentence(4), '.'));
            $todo->setDone($faker->boolean());
            $todo->setProject($faker->randomElement($projects));

            $manager->persist($todo);
            $this->addReference(self::REFERENCE_PREFIX . $i, $todo);
        }
        $manager->flush();
    }

    private function getProjects(): \Generator
    {
        for ($i = 0; $i < ProjectFixtures::NUM; ++$i) {
            yield $this->getReference(ProjectFixtures::REFERENCE_PREFIX . $i);
        }
    }

    public function getDependencies(): array
    {
        return [ProjectFixtures::class];
    }
}
