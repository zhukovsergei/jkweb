<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProjectFixtures extends Fixture
{
    public const NUM = 30;
    public const REFERENCE_PREFIX = 'project';

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < self::NUM; ++$i) {
            $project = new Project();
            $project->setName(trim($faker->sentence(4), '.'));
            $project->setManager(trim($faker->name()));
            $project->setEmail(trim($faker->email()));

            $manager->persist($project);
            $this->addReference(self::REFERENCE_PREFIX.$i, $project);
        }
        $manager->flush();
    }

    private function getProjects(): \Generator
    {
        for ($i = 0; $i < ProjectFixtures::NUM; ++$i) {
            yield $this->getReference(ProjectFixtures::REFERENCE_PREFIX.$i);
        }
    }
}
