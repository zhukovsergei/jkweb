<?php

declare(strict_types=1);

namespace App\ReadModel\Project;

use Doctrine\DBAL\Connection;

class ProjectFetcher
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function findAll(): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'id',
                'name',
                'manager',
                'email',
            )
            ->from('project')
            ->orderBy('id')
            ->executeQuery();

        return $stmt->fetchAllAssociative();
    }

}

