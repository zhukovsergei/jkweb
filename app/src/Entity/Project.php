<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ApiResource(
    collectionOperations: ['get' => [
    'normalization_context' => ['groups' => ['readProject']],
]],
    itemOperations: ['get' => [
    'normalization_context' => ['groups' => ['readProject']],
]],
)]
class Project
{
    use EntityIdTrait;

    #[ORM\Column(type: 'string', length: 255)]
    #[NotBlank(groups: ['readProject'])]
    #[Length(min: 1, groups: ['writeProject'])]
    private ?string $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[NotBlank(groups: ['readProject'])]
    #[Length(min: 1, groups: ['writeProject'])]
    private ?string $manager;

    #[ORM\Column(type: 'string', length: 255)]
    #[NotBlank(groups: ['readProject'])]
    #[Length(min: 1, groups: ['writeProject'])]
    private ?string $email;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Todo::class)]
    private Collection $todos;

    #[Pure] public function __construct()
    {
        $this->todos = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getManager(): ?string
    {
        return $this->manager;
    }

    public function setManager(?string $manager): void
    {
        $this->manager = $manager;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getTodos(): Collection
    {
        return $this->todos;
    }

    public function setTodos(Collection $todos): void
    {
        $this->todos = $todos;
    }
}
