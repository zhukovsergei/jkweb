<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity]
#[ApiResource(
    collectionOperations: [
    'get' => [
        'normalization_context' => ['groups' => ['readTodo']],
    ],
    'post' => [
        'normalization_context' => ['groups' => ['readTodo']],
    ]],
    itemOperations: [
        'get' => [
            'method' => 'get',
            'normalization_context' => ['groups' => ['readTodo']],
        ],
        'patch' => [
            'method' => 'patch',
            'normalization_context' => ['groups' => ['writeTodo']],
        ],
        'delete']
)]
class Todo
{
    use EntityIdTrait;

    #[ORM\Column(type: 'text')]
    #[NotBlank(groups: ['writeTodo'])]
    #[Length(min: 1, groups: ['writeTodo'])]
    private ?string $name;

    #[ORM\Column(type: 'boolean')]
    #[NotBlank(groups: ['writeTodo'])]
    private bool $done;

    #[ORM\ManyToOne(targetEntity: Project::class, inversedBy: 'projects')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[NotNull(groups: ['writeTodo'])]
    private ?Project $project;

    #[Pure] public function __construct()
    {
        $this->done = false;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function isDone(): bool
    {
        return $this->done;
    }

    public function setDone(bool $done): void
    {
        $this->done = $done;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): void
    {
        $this->project = $project;
    }
}
