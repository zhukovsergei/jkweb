module.exports = {

  runtimeCompiler: true,

  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg');

    svgRule.uses.clear();

    svgRule
      .use('vue-loader')
      .loader('vue-loader-v16')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
      .options({
        svgo: {
          plugins: [
            { prefixIds: true },
          ],
        },
      });
  },

  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @use "sass:math";
        `,
      },
    },
  },
};
