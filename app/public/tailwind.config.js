const plugin = require('tailwindcss/plugin')
const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        white: 'white',
        yellow: 'var(--color-yellow)',
        teal: 'var(--color-teal)',
      },
      fontSize: {
        // https://nekocalc.com/px-to-rem-converter
        xs: ['0.667rem', '0.792rem'],
        sm: ['1rem', '1.25rem'],
        base: ['1.25rem', '1.625rem'],
        lg: ['1.625rem', '2.125rem'],
        xl: ['2.5rem', '2.875rem'],
      },

      container: {
        center: true,
      },

      screens: {
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px',
        xl2: '1536px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    plugin(function({ addBase, theme }) {
      addBase({
        'h1': { fontSize: theme('fontSize.xl') },
        'h2': { fontSize: theme('fontSize.lg') },
        'h3': { fontSize: theme('fontSize.base') },
        'p': { fontSize: theme('fontSize.base') },
      })
    })
  ]
}
