import {createRouter, createWebHistory} from "vue-router";

export default createRouter({
    history: createWebHistory(),
    routes: [
        // main pages
        { path: '/', component: () => import(/* webpackChunkName: "landingPage" */ '../components/pages/LandingPage.vue') },
        { path: '/projects', component: () => import(/* webpackChunkName: "Index" */ '../components/pages/projects/Index.vue') },
        { name: 'project-show', path: '/projects/:id', component: () => import(/* webpackChunkName: "Show" */ '../components/pages/projects/Show3.vue') },

        // 404 page
        { path: '/:pathMatch(.*)*', component: () => import(/* webpackChunkName: "404" */ '../components/structure/NotFound.vue') },
    ],
    scrollBehavior(to, from, savedPosition) {
        if (to.hash) {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve({ el: to.hash, behavior: 'smooth' });
                }, 850);
            });
        }
        if (savedPosition) return savedPosition;
        return { top: 0, behavior: 'smooth' };
    },
});
