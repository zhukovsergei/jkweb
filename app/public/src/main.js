import {createApp} from 'vue'
import App from './App.vue'
import Header from './components/blocks/Header.vue';
import Footer from "./components/blocks/Footer.vue";

import './assets/styles/main.scss';

import store from './store';
import router from './router';

import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'

library.add(fas);

const API = 'http://localhost:8080/api';

const app = createApp(App)
    .component('fa', FontAwesomeIcon)
    .component('my-footer', Footer)
    .component('my-header', Header);
app.config.globalProperties.$API = API;
app.use(store);
app.use(router);

app.mount('#app');
