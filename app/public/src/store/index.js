/* eslint-disable */

import {createStore, createLogger} from 'vuex';
import axios from 'axios';

const debug = process.env.NODE_ENV !== 'production';

export default createStore({

    state: {
        hello: 'world',
    },

    mutations: {},

    actions: {},
    strict: false,
    plugins: debug ? [createLogger()] : [],
});
